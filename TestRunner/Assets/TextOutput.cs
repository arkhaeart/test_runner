﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TextOutput : MonoBehaviour
{
    string[] gameStart = new string[3] { "READY!", "SET!", "GO!" };
    string gameEnd = "GAME OVER";
    Animation anim;
    Text text;
    MyCour playing,waiting;
    // Start is called before the first frame update
    void Awake()
    {
        anim = GetComponent<Animation>();
        text = GetComponent<Text>();
        waiting = new MyCour(this, new System.Func<IEnumerator>(Waiting));
    }
    public void Start()
    {
        LevelManager.Instance.GameEnd += GameEnd;
        StartCoroutine(Playing());
    }
    void GameEnd()
    {
        text.enabled = true;
        PlayAnimation(gameEnd, Color.red);
    }
    IEnumerator Playing()
    {
        for (int i = 0; i < gameStart.Length; i++)
        {
            PlayAnimation(gameStart[i], Color.green);
            while(waiting.IsActive)
            {
                yield return null;
            }
        }
        yield return null;
        text.enabled = false;
        LevelManager.Instance.StartGame();
    }
    void PlayAnimation(string message, Color color)
    {
        text.text = message;
        text.color = color;
        waiting.Run();
        anim.Play();
    }
    public void AnimationPlayed()
    {
        waiting.Stop();
    }
    IEnumerator Waiting()
    {
        while(true)
        {
            yield return null;
        }
    }
}
