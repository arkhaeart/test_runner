﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalExit : MonoBehaviour
{
    public Vector3 exitPoint;
    Queue<Platform> inactive = new Queue<Platform>();
    private void Start()
    {
       
    }
    public void StorePlatform(Platform platform)
    {
        platform.moving.Stop();
        platform.transform.position = exitPoint;
        LoopManager.Instance.platforms.Enqueue(platform);
    }
    void RandomisePlatform(Platform platform)
    {
        //float angle = Random.Range(-5,5);
        //platform.transform.Rotate(Vector3.forward, angle);
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawSphere(exitPoint, 0.5f);
    }
}
