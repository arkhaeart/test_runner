﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Func = System.Func<System.Collections.IEnumerator>;
public class MyCour 
{
    Func func;
    Coroutine cour,subcour;
    MonoBehaviour mono;
    public MyCour(MonoBehaviour mono, Func func)
    {
        this.mono = mono;
        this.func = func;
    }
    public void Run()
    {
        Stop();
        cour = mono.StartCoroutine(Process());
    }
    IEnumerator Process()
    {
        subcour = mono.StartCoroutine(func?.Invoke());
        yield return subcour;
        cour = null;
        subcour = null;
    }
    public void Stop()
    {
        if(IsActive)
        {
            mono.StopCoroutine(cour);
            cour = null;
        }
        if(subcour!=null)
        {
            mono.StopCoroutine(subcour);
            subcour = null;
        }
    }
    public bool IsActive => cour != null;
}
