﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalEntrance : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {

        Debug.Log(collision.gameObject);

        Platform platform = collision.GetComponentInParent<Platform>();
        Debug.Log(platform);
        if (platform != null)
        {
            LevelManager.Instance.exit.StorePlatform(platform);
        }
    }
}
