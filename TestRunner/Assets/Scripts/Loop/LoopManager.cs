﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopManager : Singleton<LoopManager>
{
    public Queue<Platform> platforms = new Queue<Platform>();
    float currentHole=0;
    float desiredHole;
    public float maximumHole = 20;
    public float minPlatformLenght = 5;
    public float maxPlatformLenght = 10;
    WaitForSeconds refreshRate;
    MyCour platforming;
    private void Start()
    {
        refreshRate = new WaitForSeconds(0.2f);
        platforming = new MyCour(this, new System.Func<IEnumerator>(PlatformPlacer));
        SetDesiredHole();
        LevelManager.Instance.GameStart += GameStart;
        LevelManager.Instance.GameEnd += GameEnd;
    }
    void GameStart()
    {
        platforming.Run();
    }
    void GameEnd()
    {
        platforming.Stop();
    }
    void SetDesiredHole()
    {
        desiredHole = Random.Range( maximumHole / 2,maximumHole);
    }
    IEnumerator PlatformPlacer()
    {
        while(true)
        {
            if(currentHole>=desiredHole)
            {
                PlacePlatform();
            }
            yield return refreshRate;
            currentHole += 1;
        }
    }
    void PlacePlatform()
    {
        if (platforms.Count == 0)
            return;
        float width = RandomWidth;
        float pos = RandomPos;
        Platform platform = platforms.Dequeue();
        platform.Set(width,pos);
        platform.moving.Run();
        currentHole = 0;
        SetDesiredHole();
        desiredHole += width*1.1f;
    }
    float RandomWidth
    {
        get=> Random.Range(minPlatformLenght, maxPlatformLenght);
    }
    float RandomPos
    {
        get => Random.Range(-0.1f, 0.4f);
    }
}
