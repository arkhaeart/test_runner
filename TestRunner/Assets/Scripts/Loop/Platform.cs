﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    public Transform rightSide;
    public FailZone[] slots;
    SpriteRenderer spriteRenderer;
    public Rigidbody2D rgbd;
    public MyCour moving;
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        rgbd = GetComponent<Rigidbody2D>();
        PlaceRightSide();
        moving = new MyCour(this, new System.Func<IEnumerator>(Movement));

    }
    private void Start()
    {
        LevelManager.Instance.GameStart += GameStart;
        LevelManager.Instance.GameEnd += GameEnd;
    }
    private void GameStart()
    {
        moving.Run();
    }
    void GameEnd()
    {
        moving.Stop();
    }
    public void Set(float newWidth,float yvar)
    {
        spriteRenderer.size = new Vector2(newWidth, 0.7f);
        transform.position = transform.position + new Vector3(0, yvar, 0);
        PlaceRightSide();
        SetObstacle(newWidth);
    }
    void SetObstacle(float width)
    {
        foreach(var zone in slots)
        {
            zone.gameObject.SetActive(false);
        }
        if (width < 4)
            return;
        FailZone obst = slots[(int)Random.Range(0, 1.9f)];
        Vector3 newPos = new Vector3(Random.Range(2,width/1.5f), obst.transform.localPosition.y);
        obst.transform.localPosition = newPos;
        obst.gameObject.SetActive(true);
    }
    void PlaceRightSide()
    {
        rightSide.localPosition = new Vector2(spriteRenderer.size.x+0.35f, -0.35f);
    }
    IEnumerator Movement()
    {
        while(true)
        {

            Vector3 newPos = transform.position + new Vector3(-LevelManager.Instance.platformSpeed * 0.01f, 0);
            rgbd.MovePosition(newPos);
            
            yield return null;
        }
    }
}
