﻿#define DESKTOP
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class InputManager : Singleton<InputManager>,IPointerDownHandler,IPointerClickHandler,IPointerUpHandler
{
    public event System.Action<InputResponse> OnInputResponse;
    Coroutine holding;
    // Start is called before the first frame update
    void Start()
    {

    }

    ////Update is called once per frame
    //    void Update()
    //{
    //    if (Input.touchCount > 0)
    //    {
    //        Touch touch = Input.GetTouch(0);
    //        if (touch.phase == TouchPhase.Ended)
    //        {
    //            OnInputResponse(InputResponse.JUMP);
    //        }
    //        else if (touch.deltaTime >= holdThreshold)
    //        {
    //            OnInputResponse(InputResponse.GLIDE);
    //        }
    //    }

    //}

    public void OnPointerClick(PointerEventData eventData)
    {
        if(holding!=null)
            StopCoroutine(holding);
        OnInputResponse?.Invoke(InputResponse.JUMP);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("entered!");
        holding=StartCoroutine(ProcessPointerDown(eventData));
    }
    IEnumerator ProcessPointerDown(PointerEventData data)
    {
        int i = 0;
        while (true)
        {
            Debug.Log(i);
            if (i >= 10)
            {
                OnInputResponse?.Invoke(InputResponse.GLIDE);
                yield break; }
            yield return null;
            i++;

        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {

    }
}
