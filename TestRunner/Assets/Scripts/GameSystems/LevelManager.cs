﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LevelManager : Singleton<LevelManager>
{
    public Player player;
    public PortalEntrance entrance;
    public PortalExit exit;
    public float platformSpeed = 0.5f;
    public event System.Action GameStart;
    public event System.Action GameEnd;

    protected override void Awake()
    {
        base.Awake();
    }

    // Start is called before the first frame update
    void Start()
    {
        Screen.orientation = ScreenOrientation.LandscapeLeft;
    }
    void Restart()
    {
        SceneManager.LoadScene(0);
    }
    public void StartGame()
    {
        GameStart?.Invoke();
    }
    public void EndGame()
    {
        GameEnd?.Invoke();
        Invoke("Restart", 3);
    }
}
