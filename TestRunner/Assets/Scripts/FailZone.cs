﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CircleCollider2D))]
public class FailZone : MonoBehaviour
{
    public static event System.Action<InputResponse> OnFail;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.TryGetComponent(out Player player))
        {
            OnFail(InputResponse.FAIL);
        }
    }
}
