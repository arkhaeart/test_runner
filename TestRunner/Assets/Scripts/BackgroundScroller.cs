﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScroller : MonoBehaviour
{
    Renderer spRenderer;
    MyCour scrolling;
    public float scrollingSpeed;
    public float scrollingTime = 0;
    void Awake()
    {
        spRenderer = GetComponent<Renderer>();
        scrolling = new MyCour(this, new System.Func<IEnumerator>(Scrolling));
    }
    private void Start()
    {
        LevelManager.Instance.GameStart += GameStart;
        LevelManager.Instance.GameEnd += GameEnd;
    }
    void GameStart()
    {
        scrolling.Run();
    }
    void GameEnd()
    {
        scrolling.Stop();
    }
    IEnumerator Scrolling()
    {
        while(true)
        {
            scrollingTime += Time.deltaTime;
            Vector2 offset = new Vector2(scrollingTime * scrollingSpeed, 0);
            spRenderer.material.SetTextureOffset("_MainTex", offset);
            yield return null;
        }
    }
}
