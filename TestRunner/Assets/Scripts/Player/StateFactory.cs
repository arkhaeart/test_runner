﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateFactory 
{
    public Player player;

    DefaultState defaultState;
    FirstJumpState firstJumpState;
    SecondJumpState secondJumpState;
    GlideState glideState;
    FallingState fallingState;
    FailState failState;


    public StateFactory(Player player)
    {
        this.player = player;
    }
    public List<State> GetStates()
    {
        List<State> states = new List<State>();
        CreateStates();
        InitStates();
        states.Add(defaultState);
        states.Add(firstJumpState);
        states.Add(secondJumpState);
        states.Add(glideState);
        states.Add(fallingState);
        states.Add(failState);
        return states;

    }
    void CreateStates()
    {
        defaultState = new DefaultState(player);
        firstJumpState = new FirstJumpState(player);
        secondJumpState = new SecondJumpState(player);
        glideState = new GlideState(player);
        fallingState = new FallingState(player);
        failState = new FailState(player);
    }
    void InitStates()
    {
        State.InitOverrides(new KeyValuePair<InputResponse, State>(InputResponse.LAND, defaultState),
            new KeyValuePair<InputResponse, State>(InputResponse.FAIL, failState));
        defaultState.Init(new KeyValuePair<InputResponse, State>(InputResponse.JUMP, firstJumpState),
                            new KeyValuePair<InputResponse,State>(InputResponse.FALL,fallingState));
        firstJumpState.Init(new KeyValuePair<InputResponse, State>(InputResponse.JUMP, secondJumpState),
                            new KeyValuePair<InputResponse, State>(InputResponse.GLIDE, glideState));
        glideState.Init(new KeyValuePair<InputResponse,State>(InputResponse.FALL,fallingState)); 
    }
}
