﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Rigidbody2D rgbd;
    public State currentState;
    public Animator animator;
    List<State> states;
    // Start is called before the first frame update
    private void Awake()
    {
        rgbd = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();

    }
    void GameStart()
    {
        InputManager.Instance.OnInputResponse += Input;
        RunState();
    }
    void Start()
    {
        StateFactory factory = new StateFactory(this);
        
        FailZone.OnFail += Input;
        states = factory.GetStates();
        currentState = states[0];
        LevelManager.Instance.GameStart += GameStart;
    }
    public void Input(InputResponse response)
    {
        currentState.Input(response);
    }
    void StateChange(State to)
    {
       
        currentState = to;
        RunState();
    }
    void RunState()
    {
        currentState.StateChanged += StateChange;
        currentState.Run();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag=="Ground")
        {
            Input(InputResponse.LAND);
        }
    }
    public bool Grounded
    {
        get
        {
            if (Physics2D.Raycast(transform.position, Vector2.down, 1.5f, LayerMask.GetMask("Ground")))
            {
                return true;
            }
            else return false;
        }
    }
}
