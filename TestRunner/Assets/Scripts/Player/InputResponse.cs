﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InputResponse
{
    JUMP,
    GLIDE,
    LAND,
    FALL,
    FAIL
}