﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class State
{
    protected const string go = "Go";
    protected const string fall = "Falling";
    protected const string jump = "Jump";
    protected const string glide = "Glide";

    protected Player mono;
    
    public event System.Action<State> StateChanged;
    protected Dictionary<InputResponse, State> links = new Dictionary<InputResponse, State>();
    public static Dictionary<InputResponse, State> overrides = new Dictionary<InputResponse, State>();
    public State(Player mono)
    {
        this.mono = mono;
    }
    public static void InitOverrides(params KeyValuePair<InputResponse, State>[] links)
    {
        overrides.Clear();
        foreach(var ov in links)
        {
            overrides.Add(ov.Key, ov.Value);
        }
    }
    public void Init(params KeyValuePair<InputResponse, State>[] links)
    {
        foreach (var link in links)
        {
            this.links.Add(link.Key, link.Value);
        }
    }
    public virtual void Input(InputResponse icommand)
    {
        if(overrides.ContainsKey(icommand))
        {
            StateChange(overrides[icommand]);
        }
        else if(links.ContainsKey(icommand))
        {
            StateChange(links[icommand]);
        }
    }
    public virtual void StateChange(State to)
    {
        if (to == this)
            return;
        StateChanged?.Invoke(to);
        foreach(System.Action<State> del in StateChanged.GetInvocationList())
        {
            StateChanged -= del;
        }
    }
    public abstract void Run();
}

public class DefaultState : State
{
    public DefaultState(Player mono) : base(mono)
    {

    }

    public override void Run()
    {
        mono.animator.SetTrigger(go);
    }
}
public class FirstJumpState : State
{
    public FirstJumpState(Player mono) : base(mono)
    {
        

    }

    public override void Run()
    {
        if (!mono.Grounded)
        {
            //return;
        }
            mono.rgbd.AddForce(Vector2.up * 7, ForceMode2D.Force);
            mono.animator.SetTrigger(jump);
        
    }
}
public class SecondJumpState : State
{
    public SecondJumpState(Player mono) : base(mono)
    {
    }

    public override void Run()
    {
        mono.rgbd.AddForce(Vector2.up*7, ForceMode2D.Force);

    }
}
public class GlideState : State
{
    public GlideState(Player mono) : base(mono)
    {
    }

    public override void Run()
    {
        mono.rgbd.AddForce(Vector2.up * 0.25f, ForceMode2D.Force);
        Vector2 newGrav = new Vector2(0, -5);
        Physics2D.gravity = newGrav;
        mono.animator.SetTrigger(glide);

    }
    public override void StateChange(State to)
    {
        Vector2 baseGrav = new Vector2(0, -18);
        Physics2D.gravity = baseGrav;
        base.StateChange(to);
        
    }
}
public class FallingState : State
{
    public FallingState(Player mono) : base(mono)
    {
    }

    public override void Run()
    {
        mono.animator.SetTrigger(fall);

    }
}
public class FailState : State
{
    public FailState(Player mono) : base(mono)
    {
        

    }

    public override void Run()
    {
        
        mono.GetComponent<Collider2D>().enabled = false;
        mono.animator.SetTrigger(fall);
        Debug.Log("Game Over!");
        LevelManager.Instance.EndGame();
    }
}

